//
//  Extension.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    fileprivate func downloadedFrom(url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
            image = nil
            
            contentMode = mode
            URLCache.shared.removeAllCachedResponses()
            URLCache.shared.diskCapacity = 0
            URLCache.shared.memoryCapacity = 0
            if let cacheImage = imageCache.object(forKey: url as AnyObject) as? UIImage {
                self.image = cacheImage
                return
            }
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                DispatchQueue.main.async() {
                    imageCache.setObject(image, forKey: url as AnyObject)
                    self.image = image
                }
                }.resume()
        }
        func downloadedFrom(link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
            guard let url = URL(string: link) else { return }
            downloadedFrom(url: url, contentMode: mode)
        }
}
