//
//  Api.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import Foundation

class Api {
    
    static let shared = Api()
    
    func fetchData <T: Codable>(url: String, completion : @escaping (T?,Error?) -> ()) {
        do {
            let url = URL(string: url)!
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "GET"
    
            URLSession.shared.dataTask(with: request) { (data , resp , error) in
                guard let data = data else { completion(nil,error); return }
                do {
                    let obj = try JSONDecoder().decode(T.self, from: data)
                    completion(obj,nil)
                } catch let jsonError {
                    completion(nil,jsonError); return }
                }.resume()
        } catch { completion(nil,error); return}
    }
    
}
