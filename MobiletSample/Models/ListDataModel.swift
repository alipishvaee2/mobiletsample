//
//  ListDataModel.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import Foundation

struct ListDataModel: Codable {
    var id: Int
    var title: String
    var url: String
    var thumbnailUrl: String
}
