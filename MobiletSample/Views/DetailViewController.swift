//
//  DetailViewController.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailTitle: UILabel!
    
    var data: ListDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
    
    fileprivate func prepareView() {
        if let data = data {
            detailImage.downloadedFrom(link: data.url)
            detailTitle.text = data.title
        }
    }
    
}
