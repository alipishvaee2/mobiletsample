//
//  MainCollectionViewController.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import UIKit

private let reuseIdentifier = "cellId"


class MainCollectionViewController: UICollectionViewController {
    
    let viewModel = ListDataViewModel()
    var dataList = [ListDataModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.requestUrl = "https://my-json-server.typicode.com/amosavian/restdemo/1-10"

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout, let collectionView = collectionView {
            let w = collectionView.frame.width - 20
            flowLayout.estimatedItemSize = CGSize(width: w, height: 200)
        }
        
        viewModel.listFetchDone = { [weak self]  result in
            DispatchQueue.main.async {
                guard let result = result else { return }
                self?.dataList.append(contentsOf: result)
                self?.collectionView.reloadData()
            }
        }
        
        viewModel.onErrorHandling = { error in
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: error, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        cell.data = dataList[indexPath.row]
        return cell
    }
}

extension MainCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let fromCount = dataList.count+1
        let toCount = dataList.count+10
        let currentCount = indexPath.row + 1
        if  currentCount % 10 == 0 && currentCount == dataList.count {
            viewModel.requestUrl = "https://my-json-server.typicode.com/amosavian/restdemo/\(fromCount)-\(toCount)"
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.bounds.width, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVC = storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        detailVC.data = dataList[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    
}
