//
//  CollectionViewCell.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    fileprivate var imageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    fileprivate var title: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    var data: ListDataModel? {
        didSet {
            guard let data = data else { return }
            title.text = data.title
            imageView.downloadedFrom(link: data.thumbnailUrl)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        [title, imageView].forEach({ addSubview($0) })
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
        title.translatesAutoresizingMaskIntoConstraints = false
        title.topAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        title.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        title.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        title.widthAnchor.constraint(equalToConstant: 150).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
