//
//  ListDataViewModel.swift
//  MobiletSample
//
//  Created by Ali Pishvaee on 9/28/19.
//  Copyright © 2019 Ali Pishvaee. All rights reserved.
//

import Foundation

class ListDataViewModel {
    var listFetchDone:(([ListDataModel]?) -> Void)?
    var onErrorHandling : ((String?) -> Void)?
    var requestUrl: String? {
        didSet {
            fetchListData()
        }
    }
    
    fileprivate func fetchListData() {
        Api.shared.fetchData(url: requestUrl!, completion: { (result: [ListDataModel]?, error) in
            guard let result = result else { self.onErrorHandling?(error!.localizedDescription); return }
            self.listFetchDone?(result)
        })
    }
}
